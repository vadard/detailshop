﻿using System;
using System.Collections.Generic;

namespace Data.Models
{
    public partial class Parts
    {
        public Guid PartId { get; set; }
        public string Title { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string Kuzov { get; set; }
        public string Oem { get; set; }
        public string Engine { get; set; }
        public string Year { get; set; }
        public string ExtColor { get; set; }
        public string Comment { get; set; }
        public int? Price { get; set; }
        public string Photos { get; set; }
        public Guid? DonorId { get; set; }
        public string PartKod { get; set; }
        public string StoreName { get; set; }
        public string Status { get; set; }
    }
}
