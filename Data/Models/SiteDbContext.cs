﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Data.Models
{
    public class SiteDbContext : DbContext
    {
        public SiteDbContext()
        {
        }

        public SiteDbContext(DbContextOptions<SiteDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Parts> Parts { get; set; }
        public virtual DbSet<Models> Models { get; set; }

        // Unable to generate entity type for table 'dbo.Models'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.Donors'. Please see the warning messages.

//        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
//        {
//            if (!optionsBuilder.IsConfigured)
//            {
//#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
//                optionsBuilder.UseSqlServer("Data Source=81.176.228.209,14143;Initial Catalog=site1_db;Persist Security Info=True;User ID=vladimir_site1;Password=123456;");
//            }
//        }
//
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Models>(entity => { entity.HasKey(e => e.Title).ForSqlServerIsClustered(); });
            modelBuilder.Entity<Parts>(entity =>
            {
                entity.HasKey(e => e.PartId)
                    .ForSqlServerIsClustered(false);

                entity.Property(e => e.PartId)
                    .HasColumnName("PartID")
                    .ValueGeneratedNever();

                entity.Property(e => e.Comment).HasMaxLength(400);

                entity.Property(e => e.DonorId).HasColumnName("DonorID");

                entity.Property(e => e.Engine).HasMaxLength(50);

                entity.Property(e => e.ExtColor).HasMaxLength(50);

                entity.Property(e => e.Kuzov).HasMaxLength(50);

                entity.Property(e => e.Make).HasMaxLength(50);

                entity.Property(e => e.Model).HasMaxLength(50);

                entity.Property(e => e.Oem).HasMaxLength(200);

                entity.Property(e => e.PartKod).HasMaxLength(50);

                entity.Property(e => e.Status).HasMaxLength(50);

                entity.Property(e => e.StoreName).HasMaxLength(50);

                entity.Property(e => e.Title).HasMaxLength(255);

                entity.Property(e => e.Year).HasMaxLength(50);
            });
        }
    }
}