using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Data.Services.Makers
{
    public interface IMakersService
    {
        Task<List<MakerContract>> GetAll();
    }
}