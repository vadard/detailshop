namespace Data.Services.Makers
{
    public class MakerContract
    {
        public string Name { get; set; }
        public string PhotoUrl { get; set; }
    }
}