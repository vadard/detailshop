using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Data.Models;
using Microsoft.EntityFrameworkCore;

namespace Data.Services.Makers
{
    public class MakersService : IMakersService
    {
        private readonly SiteDbContext _context;

        public MakersService(SiteDbContext context)
        {
            _context = context;
        }

        public async Task<List<MakerContract>> GetAll()
        {
            List<Data.Models.Models> modelsPhotos = _context.Models.ToList();
            return _context.Parts.ToList().GroupBy(x => x.Make).Select(x => x.First()).Select(x => new MakerContract
            {
                Name = x.Make,
                PhotoUrl = modelsPhotos.FirstOrDefault(p => string.Equals(p.Title, x.Make, StringComparison.CurrentCultureIgnoreCase))?.Url
            }).ToList();
        }
    }
}