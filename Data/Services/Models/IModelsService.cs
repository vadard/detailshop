using System.Collections.Generic;
using System.Threading.Tasks;

namespace Data.Services.Models
{
    public interface IModelsService
    {
        Task<IEnumerable<ModelContract>> GetAll();
    }
}