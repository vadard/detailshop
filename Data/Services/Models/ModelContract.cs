using Data.Services.Makers;

namespace Data.Services.Models
{
    public class ModelContract
    {
        public string ModelName { get; set; }
        public MakerContract Maker { get; set; }
    }
}