using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;
using Data.Models;
using Data.Services.Makers;
using Microsoft.EntityFrameworkCore;

namespace Data.Services.Models
{
    public class ModelService : IModelsService
    {
        private readonly SiteDbContext _context;

        public ModelService(SiteDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<ModelContract>> GetAll()
        {
            return _context.Parts.Select(x => new ModelContract
            {
                ModelName = x.Model,
                Maker = new MakerContract {Name = x.Make}
            }).GroupBy(x => new {x.Maker.Name, x.ModelName}).Select(x => x.First());
        }
    }
}