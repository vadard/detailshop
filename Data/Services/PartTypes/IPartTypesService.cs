using System.Collections.Generic;
using System.Threading.Tasks;

namespace Data.Services.PartTypes
{
    public interface IPartTypesService
    {
       Task<IEnumerable<PartTypeContract>> GetAllPartTypes();
    }
}