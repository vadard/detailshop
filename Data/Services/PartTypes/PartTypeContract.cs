using Data.Services.Makers;
using Data.Services.Models;

namespace Data.Services.PartTypes
{
    public class PartTypeContract
    {
        public string PartTypeName { get; set; }
        public MakerContract Maker { get; set; }
        public ModelContract Model { get; set; }
    }
}