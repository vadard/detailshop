using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Data.Models;
using Data.Services.Makers;
using Data.Services.Models;

namespace Data.Services.PartTypes
{
    public class PartTypesService : IPartTypesService
    {
        private readonly SiteDbContext _siteDbContext;

        public PartTypesService(SiteDbContext siteDbContext)
        {
            _siteDbContext = siteDbContext;
        }

        public async Task<IEnumerable<PartTypeContract>> GetAllPartTypes()
        {
            return _siteDbContext.Parts.Select(x => new PartTypeContract
            {
                PartTypeName = x.Title,
                Maker = new MakerContract
                {
                    Name = x.Make
                },
                Model = new ModelContract
                {
                    ModelName = x.Model,
                    Maker = new MakerContract
                    {
                        Name = x.Make
                    }
                }
            }).GroupBy(x => new {x.PartTypeName, x.Maker.Name, x.Model.ModelName}).Select(x => x.First());
        }
    }
}