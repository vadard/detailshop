using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Data.Services.Parts
{
    public interface IPartsService
    {
        Task<IEnumerable<PartContract>> GetAllParts();
        Task<PartContract> GetPart(Guid partId);
    }
}