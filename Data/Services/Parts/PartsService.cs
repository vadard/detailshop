using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Data.Models;
using Microsoft.EntityFrameworkCore;

namespace Data.Services.Parts
{
    public class PartsService : IPartsService
    {
        private readonly SiteDbContext _context;

        public PartsService(SiteDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<PartContract>> GetAllParts()
        {
            return _context.Parts.Select(x => new PartContract
            {
                Id = x.PartId,
                Title = x.Title,
                Make = x.Make,
                Model = x.Model,
                Kuzov = x.Kuzov,
                Oem = x.Oem,
                Engine = x.Engine,
                Year = x.Year,
                ExtColor = x.ExtColor,
                Comment = x.Comment,
                Price = x.Price,
                Photos = x.Photos,
                DonorId = x.DonorId,
                PartKod = x.PartKod,
                StoreName = x.StoreName,
                Status = x.Status
            });
        }

        public Task<PartContract> GetPart(Guid partId)
        {
            Data.Models.Parts part = _context.Parts.FirstOrDefault(x => x.PartId == partId);
            return Task.FromResult(new PartContract
            {
                Id = part.PartId,
                Title = part.Title,
                Make = part.Make,
                Model = part.Model,
                Kuzov = part.Kuzov,
                Oem = part.Oem,
                Engine = part.Engine,
                Year = part.Year,
                ExtColor = part.ExtColor,
                Comment = part.Comment,
                Price = part.Price,
                Photos = part.Photos,
                DonorId = part.DonorId,
                PartKod = part.PartKod,
                StoreName = part.StoreName,
                Status = part.Status
            });
        }
    }
}