﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Data.Services.Parts;
using Microsoft.AspNetCore.Mvc;
using Web.Helpers.Makers;
using Web.Helpers.Models;
using Web.Helpers.Parts;
using Web.Helpers.PartTypes;
using Web.Models;

namespace Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly IMakersHelper _makersHelper;
        private readonly IModelsHelper _modelsHelper;
        private readonly IPartTypesHelper _partTypesHelper;
        private readonly IPartsHelper _partsHelper;

        public HomeController(IMakersHelper makersHelper, IModelsHelper modelsHelper, IPartTypesHelper partTypesHelper, IPartsHelper partsHelper)
        {
            _makersHelper = makersHelper;
            _modelsHelper = modelsHelper;
            _partTypesHelper = partTypesHelper;
            _partsHelper = partsHelper;
        }

        [ResponseCache(Location = ResponseCacheLocation.Any, Duration = 300)]
        public async Task<IActionResult> Index()
        {
            CatalogViewModel viewModel = new CatalogViewModel();
            viewModel.Makers = await _makersHelper.GetMakers();
            viewModel.Models = await _modelsHelper.GetViewModels();
            viewModel.AllPartTypes = await _partTypesHelper.GetPartTypeViewModel();
            viewModel.GroupedPartTypes = await _partTypesHelper.GetGroupedPartTypeViewModel();
            return View(viewModel);
        }

        [HttpGet("makers/{maker}/models/{models}/detailtype/{detailtype}/kuzovs/{kuzovs}/motors/{motors}")]
        public async Task<ActionResult> Search(string maker, string models, string detailType, string kuzovs, string motors)
        {
            SearchViewModel viewModel = new SearchViewModel();

            IEnumerable<PartContract> parts = await _partsHelper.GetAllParts();
            viewModel.Makers = await _makersHelper.GetMakers();
            detailType = detailType.Replace('_', ' ');
            if (maker != "all")
            {
                parts = parts.Where(x => x.Make == maker);
                viewModel.SelectedMaker = maker;
                viewModel.Models = (await _modelsHelper.GetModels())
                    .Where(x => x.Maker.Name.ToLower().Equals(maker.ToLower()))
                    .Select(x => x.ModelName);
                viewModel.Kuzovs = parts.GroupBy(x => x.Kuzov).Select(x => x.First().Kuzov);
                viewModel.Motors = parts.GroupBy(x => x.Engine).Select(x => x.First().Engine);
            }

            if (models != "all")
            {
                viewModel.SelectedModels = models.Split("&");
                parts = parts.Where(x => viewModel.SelectedModels.Select(s => s.ToLower()).Contains(x.Model.ToLower()));
                viewModel.DetailTypes = (await _partTypesHelper.GetGroupedPartTypes()).Where(x =>
                        viewModel.SelectedModels.Select(s => s.ToLower()).Contains(x.Model.ModelName.ToLower()))
                    .GroupBy(p => p.PartTypeName.Substring(0, 1).ToLower())
                    .Select(p => new PartTypeViewModel
                    {
                        StartWord = p.Key,
                        PartTypes = p.GroupBy(x => x.PartTypeName).Select(x => x.First())
                    });
                viewModel.Kuzovs = parts.GroupBy(x => x.Kuzov).Select(x => x.First().Kuzov);
                viewModel.Motors = parts.GroupBy(x => x.Engine).Select(x => x.First().Engine);
            }
            else
            {
                viewModel.DetailTypes = (await _partTypesHelper.GetGroupedPartTypes())
                    .GroupBy(p => p.PartTypeName.Substring(0, 1).ToLower())
                    .Select(p => new PartTypeViewModel
                    {
                        StartWord = p.Key,
                        PartTypes = p.GroupBy(x => x.PartTypeName).Select(x => x.First())
                    });
            }

            if (detailType != "all")
            {
                parts = parts.Where(x => x.Title.ToLower() == detailType.Replace('_', ' ').ToLower());
                viewModel.SelectedDetailType = detailType;
                viewModel.Kuzovs = parts.GroupBy(x => x.Kuzov).Select(x => x.First().Kuzov);
                viewModel.Motors = parts.GroupBy(x => x.Engine).Select(x => x.First().Engine);
            }

            if (kuzovs != "all")
            {
                viewModel.SelectedKuzovs = kuzovs.Split('&');
                parts = parts.Where(x => viewModel.SelectedKuzovs.Contains(x.Kuzov));
                viewModel.Motors = parts.GroupBy(x => x.Engine).Select(x => x.First().Engine);
            }

            if (motors != "all")
            {
                viewModel.SelectedMotors = motors.Split('&');
                parts = parts.Where(x => viewModel.SelectedMotors.Select(s => s.ToLower()).Contains(x.Engine.ToLower()));
            }

            viewModel.Parts = parts;
            return View(viewModel);
        }

        [HttpGet("parts/{partId}")]
        public async Task<ActionResult> Part(string partId)
        {
            PartContract part = await _partsHelper.GetPart(new Guid(partId));
            return View(part);
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }


        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel {RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier});
        }
    }
}