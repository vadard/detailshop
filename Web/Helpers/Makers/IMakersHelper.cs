using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Data.Services.Makers;
using Web.Models;

namespace Web.Helpers.Makers
{
    public interface IMakersHelper
    {
        Task<IEnumerable<MakerContract>> GetMakers();
    }
}