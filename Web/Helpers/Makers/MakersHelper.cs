using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Data.Services.Makers;
using Microsoft.Extensions.Caching.Memory;
using Web.Models;

namespace Web.Helpers.Makers
{
    public class MakersHelper : IMakersHelper
    {
        private readonly IMakersService _makersService;
        private readonly IMemoryCache _memoryCache;
        private const string _cacheKey = "makers";

        public MakersHelper(IMakersService makersService, IMemoryCache memoryCache)
        {
            _makersService = makersService;
            _memoryCache = memoryCache;
        }

        public async Task<IEnumerable<MakerContract>> GetMakers()
        {
            List<MakerContract> result;
            if (!_memoryCache.TryGetValue(_cacheKey, out result))
            {
                //IEnumerable<MakerContract> makers = await _makersService.GetAll();
                result =  await _makersService.GetAll();;

                _memoryCache.Set(_cacheKey, result, new MemoryCacheEntryOptions
                {
                    AbsoluteExpirationRelativeToNow = TimeSpan.FromHours(5)
                });
            }

            return result;
        }
    }
}