using System.Collections.Generic;
using System.Threading.Tasks;
using Data.Services.Models;
using Web.Models;

namespace Web.Helpers.Models
{
    public interface IModelsHelper
    {
        Task<IEnumerable<ModelContract>> GetModels();
        Task<IEnumerable<ModelViewModel>> GetViewModels();
    }
}