using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Data.Services.Models;
using Microsoft.Extensions.Caching.Memory;
using Web.Models;

namespace Web.Helpers.Models
{
    public class ModelsHelper : IModelsHelper
    {
        private readonly IModelsService _modelsService;
        private readonly IMemoryCache _memoryCache;
        private const string _cacheKeyModels = "models";
        private const string _cacheKeyView = "modelsView";

        public ModelsHelper(IModelsService modelsService, IMemoryCache memoryCache)
        {
            _modelsService = modelsService;
            _memoryCache = memoryCache;
        }

        public async Task<IEnumerable<ModelContract>> GetModels()
        {
            List<ModelContract> result;
            if (!_memoryCache.TryGetValue(_cacheKeyModels, out result))
            {
                result = (await _modelsService.GetAll()).ToList();
                _memoryCache.Set(_cacheKeyModels, result, new MemoryCacheEntryOptions
                {
                    AbsoluteExpirationRelativeToNow = TimeSpan.FromHours(5)
                });
            }

            return result;
        }

        public async Task<IEnumerable<ModelViewModel>> GetViewModels()
        {
            List<ModelViewModel> result;
            if (!_memoryCache.TryGetValue(_cacheKeyView, out result))
            {
                result = (await GetModels()).Select(x => new ModelViewModel
                {
                    ModelName = x.ModelName,
                    MakerName = x.Maker.Name
                }).ToList();

                _memoryCache.Set(_cacheKeyView, result, new MemoryCacheEntryOptions
                {
                    AbsoluteExpirationRelativeToNow = TimeSpan.FromHours(5)
                });
            }

            return result;
        }
    }
}