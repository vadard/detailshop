using System.Collections.Generic;
using System.Threading.Tasks;
using Data.Services.PartTypes;
using Web.Models;

namespace Web.Helpers.PartTypes
{
    public interface IPartTypesHelper
    {
        Task<IEnumerable<PartTypeContract>> GetAllPartTypes();
        Task<IEnumerable<PartTypeContract>> GetGroupedPartTypes();
        Task<IEnumerable<PartTypeViewModel>> GetPartTypeViewModel();
        Task<IEnumerable<GropedPartTypesViewModel>> GetGroupedPartTypeViewModel();
    }
}