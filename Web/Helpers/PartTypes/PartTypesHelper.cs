using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Data.Services.PartTypes;
using Microsoft.Extensions.Caching.Memory;
using Web.Models;

namespace Web.Helpers.PartTypes
{
    public class PartTypesHelper : IPartTypesHelper
    {
        private readonly IPartTypesService _partTypesService;
        private readonly IMemoryCache _memoryCache;
        private const string _cacheKey = "partTypes";
        private const string _cacheKeyGrouped = "groupedPartType";

        public PartTypesHelper(IPartTypesService partTypesService, IMemoryCache memoryCache)
        {
            _partTypesService = partTypesService;
            _memoryCache = memoryCache;
        }

        public async Task<IEnumerable<PartTypeContract>> GetAllPartTypes()
        {
            IEnumerable<PartTypeContract> partTypes = await _partTypesService.GetAllPartTypes();
            return partTypes.GroupBy(x => x.PartTypeName).Select(x => x.First());
        }

        public async Task<IEnumerable<PartTypeContract>> GetGroupedPartTypes()
        {
            IEnumerable<PartTypeContract> partTypes = await _partTypesService.GetAllPartTypes();
            return partTypes.GroupBy(x => new {x.Maker.Name, x.Model.ModelName, x.PartTypeName}).Select(x => x.First());
        }

        public async Task<IEnumerable<PartTypeViewModel>> GetPartTypeViewModel()
        {
            List<PartTypeViewModel> result;
            //if (!_memoryCache.TryGetValue(_cacheKey, out result))
            {
                result = (await GetAllPartTypes()).GroupBy(x => x.PartTypeName.Substring(0, 1).ToLower()).Select(x =>
                    new PartTypeViewModel
                    {
                        StartWord = x.Key,
                        PartTypes = x
                    }).OrderBy(x => x.StartWord).ToList();

//                _memoryCache.Set(_cacheKey, result, new MemoryCacheEntryOptions
//                {
//                    AbsoluteExpirationRelativeToNow = TimeSpan.FromHours(5)
//                });
            }

            return result;
        }

        public async Task<IEnumerable<GropedPartTypesViewModel>> GetGroupedPartTypeViewModel()
        {
            List<GropedPartTypesViewModel> result;
            if (!_memoryCache.TryGetValue(_cacheKeyGrouped, out result))
            {
                result = (await GetGroupedPartTypes()).GroupBy(x => new {x.Maker.Name, x.Model.ModelName})
                    .Select(x => new GropedPartTypesViewModel
                    {
                        Maker = x.Key.Name,
                        Model = x.Key.ModelName,
                        Parts = x.GroupBy(p => p.PartTypeName.Substring(0, 1).ToLower()).Select(p => new PartTypeViewModel
                        {
                            StartWord = p.Key,
                            PartTypes = p
                        })
                    }).ToList();
                _memoryCache.Set(_cacheKey, result, new MemoryCacheEntryOptions
                {
                    AbsoluteExpirationRelativeToNow = TimeSpan.FromHours(5)
                });
            }

            return result;
        }
    }
}