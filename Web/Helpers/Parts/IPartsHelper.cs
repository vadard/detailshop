using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Data.Services.Parts;

namespace Web.Helpers.Parts
{
    public interface IPartsHelper
    {
        Task<IEnumerable<PartContract>> GetAllParts();
        Task<PartContract> GetPart(Guid partId);
    }
}