using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Data.Services.Parts;

namespace Web.Helpers.Parts
{
    public class PartsHelper : IPartsHelper
    {
        private readonly IPartsService _partsService;

        public PartsHelper(IPartsService partsService)
        {
            _partsService = partsService;
        }

        public Task<IEnumerable<PartContract>> GetAllParts()
        {
            return _partsService.GetAllParts();
        }

        public Task<PartContract> GetPart(Guid partId)
        {
            return _partsService.GetPart(partId);
        }
    }
}