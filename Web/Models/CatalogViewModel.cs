using System.Collections;
using System.Collections.Generic;
using Data.Services.Makers;
using Data.Services.Models;
using Data.Services.PartTypes;

namespace Web.Models
{
    public class CatalogViewModel
    {
        public IEnumerable<MakerContract> Makers { get; set; }
        public IEnumerable<ModelViewModel> Models { get; set; }
        public IEnumerable<PartTypeViewModel> AllPartTypes { get; set; }
        public IEnumerable<GropedPartTypesViewModel> GroupedPartTypes { get; set; }
    }
}