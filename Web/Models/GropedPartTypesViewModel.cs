using System.Collections;
using System.Collections.Generic;

namespace Web.Models
{
    public class GropedPartTypesViewModel
    {
        public string Maker { get; set; }
        public string Model { get; set; }
        public IEnumerable<PartTypeViewModel> Parts { get; set; }
    }
}