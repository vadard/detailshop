using System.Collections.Generic;
using Data.Services.PartTypes;

namespace Web.Models
{
    public class PartTypeViewModel
    {
        public string StartWord { get; set; }
        public IEnumerable<PartTypeContract> PartTypes { get; set; }
    }
}