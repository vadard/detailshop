using System.Collections;
using System.Collections.Generic;
using Data.Services.Makers;
using Data.Services.Parts;

namespace Web.Models
{
    public class SearchViewModel
    {
        public IEnumerable<PartContract> Parts { get; set; } = new List<PartContract>();
        public string SelectedMaker { get; set; }
        public IEnumerable<MakerContract> Makers { get; set; } = new List<MakerContract>();
        public IEnumerable<string> SelectedModels { get; set; }
        public IEnumerable<string> Models { get; set; } = new List<string>();
        public string SelectedDetailType { get; set; }
        public IEnumerable<PartTypeViewModel> DetailTypes { get; set; } = new List<PartTypeViewModel>();
        public IEnumerable<string> SelectedKuzovs { get; set; }
        public IEnumerable<string> Kuzovs { get; set; } = new List<string>();
        public IEnumerable<string> SelectedMotors { get; set; }
        public IEnumerable<string> Motors { get; set; } = new List<string>();
    }
}