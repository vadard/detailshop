﻿using System.IO.Compression;
using Data.Models;
using Data.Services.Makers;
using Data.Services.Models;
using Data.Services.Parts;
using Data.Services.PartTypes;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Web.Helpers.Makers;
using Web.Helpers.Models;
using Web.Helpers.Parts;
using Web.Helpers.PartTypes;

namespace Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddScoped<IMakersService, MakersService>();
            services.AddScoped<IModelsService, ModelService>();
            services.AddScoped<IMakersHelper, MakersHelper>();
            services.AddScoped<IModelsHelper, ModelsHelper>();
            services.AddScoped<IPartTypesService, PartTypesService>();
            services.AddScoped<IPartTypesHelper, PartTypesHelper>();
            services.AddScoped<IPartsService, PartsService>();
            services.AddScoped<IPartsHelper, PartsHelper>();

            services.AddDbContext<SiteDbContext>(options =>
            {
                options.UseSqlServer(
                    "Data Source=81.176.228.209,14143;Initial Catalog=site1_db;Persist Security Info=True;User ID=vladimir_site1;Password=123456;");
            });

            // добавляем сервис компрессии
            services.AddResponseCompression();
            services.Configure<GzipCompressionProviderOptions>(options =>
            {
                options.Level = CompressionLevel.Optimal;
            });
            
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Latest);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();
            app.UseCookiePolicy();
            
            // подключаем компрессию
            app.UseResponseCompression();
            
           

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "parts",
                    template: "maker/{maker}",
                    defaults: new {controller = "Home", action = "Search"});

                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
            
        }
    }
}