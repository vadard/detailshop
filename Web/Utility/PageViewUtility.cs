using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using Microsoft.EntityFrameworkCore.Internal;
using Web.Models;

namespace Web.Utility
{
    public static class PageViewUtility
    {
        public static string GetSearchUrl(string maker, IEnumerable<string> models, string detailType, IEnumerable<string> kuzovs,
            IEnumerable<string> motors)
        {
            string model = models != null ? models.Join("&") : "all";
            string kuzov = kuzovs != null ? kuzovs.Join("&") : "all";
            string motor = motors != null ? motors.Join("&") : "all";
            return $"/makers/{maker ?? "all"}/models/{model}/detailtype/{detailType ?? "all"}/kuzovs/{kuzov}/motors/{motor}";
        }
    }
}