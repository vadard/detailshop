﻿$(function () {
    
    
    $('#mark-model-btn').click(function () {
        $('#parts-block').hide();
        $('#mark-model-block').show();
    });
    $('#parts-btn').click(function () {
        $('#mark-model-block').hide();
        $('#parts-block').show();

    });


    $('.mark-item').click(function () {
        var maker = $(this).attr('data-maker');
        if (!window.location.changing) {
            window.location.hash = maker;
        }

        $('.model-item').filter(function (i, e) {
            return maker != e.dataset.maker;
        }).hide();

        $('.model-item').filter(function (i, e) {
            return maker == e.dataset.maker;
        }).show();

        $('.mark-item').hide();

        
        
        return false;
    });

    $('.model-item').click(function () {
        var maker = $(this).attr('data-maker');
        var model = $(this).attr('data-model');

        if (!window.location.changing) {
            window.location.hash = maker + '/' + model;
        }


        $('#all-parts-block').hide();
        $('#grouped-parts-block').show();
        $('.maker-model-parts').hide();
        $('.maker-model-parts[data-maker="' + maker + '"][data-model="' + model + '"]').show();


        $().button('toggle')
        $('#mark-model-block').hide();
        $('#parts-block').show();
        $('#parts-btn').button('toggle');


        return false;
    });


    $(window).on('hashchange', function () {
        if (window.location.changing) return;
        window.location.changing = true;
        var hash = window.location.hash.replace('#', '').split('/');
        if (hash[0] == '') {
            $('.model-item').hide();
            $('.mark-item').show();
            
        }

        if (hash.length == 1 && hash[0] != '') {
            $('#parts-block').hide();
            $('#mark-model-block').show();
            $('.mark-item[data-maker="' + hash[0] + '"]').click();
            $('#bread-block').show();
            $('#bread-maker').show();
            $('#bread-maker').attr('href','/#'+hash[0]);
            $('#bread-maker').text(hash[0].toUpperCase());
        }

        if (hash.length == 2) {
            $('#parts-block').show();
            $('#mark-model-block').hide();

            $('#bread-block').show();
            $('#bread-maker').show();
            $('#bread-maker').attr('href','/#'+hash[0]);
            $('#bread-maker').text(hash[0].toUpperCase());
            $('#bread-model').show();
            $('#bread-model').attr('href','/#'+hash[0]+'/'+hash[1]);
            $('#bread-model').text(hash[1].toUpperCase().replace('%20',' '));
        }
        //   if(hash.length==1 && hash[0])
        // if(!hash[1]){
        //     $('.search-history a.js-model:visible').trigger('click');
        // }
        // if(!hash[0]){
        //     $('.search-history a.js-mark:visible').trigger('click');
        // }
        // if(hash[0]){
        //     $('.search-rama-frame.models a[data-mark="'+hash[0]+'"]:first').trigger('click');
        //     if(hash[1]){
        //         $('.search-rama-frame.models [data-mark="'+hash[0]+'"] a[data-model="'+hash[1]+'"]').trigger('click');
        //     }
        // }
        window.location.changing = false;
    }).trigger('hashchange');

    $('.keep-open').click(function (e) {
        if (/input|label/i.test(e.target.tagName)) {
            var parent = $(e.target).parent();
            if (parent.hasClass('checkbox')) {
                var checkbox = parent.find('input[type=checkbox]');
                checkbox.prop("checked", !checkbox.prop("checked"));
                return false;
            }
        }
        if (e.target.tagName.toLowerCase() === 'button')
            return false;
    });

    $('#btn-model-filter').click(function () {
        var models = $('.model-checkbox>label>input[type=checkbox]:checked').map(function () {
            return $(this).attr('value');
        }).toArray().join('&');
        models = models.length == 0 ? "all" : models;
        var pathArray = window.location.pathname.split('models/');
        window.location.pathname = pathArray[0] + 'models/' + models + pathArray[1].substring(pathArray[1].indexOf('/'))
    });
    $('.btn-model-all').click(function () {
        $('.model-checkbox>label>input[type=checkbox]').attr('checked', 'checked');
    });

    $('#btn-kuzov-filter').click(function () {
        var kuzovs = $('.kuzov-checkbox>label>input[type=checkbox]:checked').map(function () {
            return $(this).attr('value');
        }).toArray().join('&');
        kuzovs = kuzovs.length == 0 ? "all" : kuzovs;
        var pathArray = window.location.pathname.split('kuzovs/');
        window.location.pathname = pathArray[0] + 'kuzovs/' + kuzovs + pathArray[1].substring(pathArray[1].indexOf('/'))
    });

    $('.btn-kuzov-all').click(function () {
        $('.kuzov-checkbox>label>input[type=checkbox]').attr('checked', 'checked');
    });

    $('#btn-motor-filter').click(function () {
        var motors = $('.motor-checkbox>label>input[type=checkbox]:checked').map(function () {
            return $(this).attr('value');
        }).toArray().join('&');
        motors = motors.length == 0 ? "all" : motors;
        var pathArray = window.location.pathname.split('motors/');
        window.location.pathname = pathArray[0] + 'motors/' + motors;
    });
    $('.btn-motor-all').click(function () {
        $('.motor-checkbox>label>input[type=checkbox]').attr('checked', 'checked');
    });
    
    
});